resource "google_project_service" "cloudresourcemanager_api" {
  project            = var.project
  service            = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "vpcaccess_api" {
  project            = var.project
  service            = "vpcaccess.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "compute_api" {
  project            = var.project
  service            = "compute.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "cloudrun_api" {
  project            = var.project
  service            = "run.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "redis_api" {
  project            = var.project
  service            = "redis.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "secretmanager_api" {
  project            = var.project
  service            = "secretmanager.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "servicenetworking_api" {
  project            = var.project
  service            = "servicenetworking.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "sqladmin_api" {
  project            = var.project
  service            = "sqladmin.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}

resource "google_project_service" "cloudbuild_api" {
  project            = var.project
  service            = "cloudbuild.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_service.cloudresourcemanager_api]
}
