variable "project" {
  description = "Project identifier"
  type        = string
}

variable "name" {
  description = "Webhook name"
}

variable "environment_name" {
  description = "Environment name"
}

variable "branch_name" {
  description = "Branch to track"
}

variable "repo_name" {
  description = "Repository name"
}

variable "csr_repo_name" {
  description = "Repository to track (Cloud Source Repositories)"
}