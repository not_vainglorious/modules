resource "google_cloudbuild_trigger" "build_trigger" {
  name = var.name

  trigger_template {
    project_id  = var.project
    branch_name = var.branch_name
    repo_name   = var.csr_repo_name
  }

  substitutions = {
    _ENV     = var.environment_name
    _PROJECT = var.repo_name
  }

  filename = "cloudbuild.yaml"
}