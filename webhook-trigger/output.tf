output "id" {
  value = google_cloudbuild_trigger.build_trigger.id
}

output "image" {
  value = "gcr.io/${var.project}/${var.environment_name}-${var.repo_name}"
}