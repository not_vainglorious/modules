locals {
  secret_manager_secret_name = "${var.name}-secret"

  username = "${var.name}-user"
  password = random_password.password.result
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

/*
 * SQL database setup
 */

resource "google_sql_database" "database" {
  project  = var.project
  name     = var.name
  instance = var.instance_name
}

resource "google_sql_user" "user" {
  name     = local.username
  password = local.password
  instance = var.instance_name
  type     = "BUILT_IN"
}

/*
 * Saving connection string into secret manager
 */

resource "google_secret_manager_secret" "secret" {
  secret_id = local.secret_manager_secret_name

  replication {
    automatic = true
  }

  depends_on = [google_sql_database.database]
}

resource "google_secret_manager_secret_version" "secret_version_data" {
  secret      = google_secret_manager_secret.secret.name
  secret_data = "${var.instance_driver}://${local.username}:${local.password}@${var.instance_connection}:${var.instance_port}/${var.name}"

  depends_on = [google_secret_manager_secret.secret, google_sql_user.user]
}