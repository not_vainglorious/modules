variable "project" {
  description = "Project identifier"
  type        = string
}

variable "network" {
  description = "Network name"
  type        = string
}

variable "name" {
  description = "Database name"
  type        = string
}

variable "instance_name" {
  description = "Database instance name"
  type        = string
}

variable "instance_driver" {
  description = "Database driver"
}


variable "instance_connection" {
  description = "Database connection"
}

variable "instance_port" {
  description = "Database port"
}