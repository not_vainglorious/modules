locals {
  secret_manager_secret_name = "${var.name}-secret"

  allocated_ip_range = "${var.name}-ip-alloc"
}

data "google_compute_network" "private_network" {
  name = var.network
}

/*
 * Redis
 */

resource "google_compute_global_address" "private_ip_alloc" {
  name          = local.allocated_ip_range
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = data.google_compute_network.private_network.id
}

resource "google_service_networking_connection" "memorystore_connection" {
  network                 = data.google_compute_network.private_network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_alloc.name]
}

resource "google_redis_instance" "instance" {
  project = var.project
  region  = var.region

  display_name  = var.display_name
  name          = var.name
  redis_version = "REDIS_5_0"

  tier           = var.tier
  memory_size_gb = var.memory_size_gb

  authorized_network = data.google_compute_network.private_network.id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  auth_enabled = false

  depends_on = [google_service_networking_connection.memorystore_connection]
}

/*
 * Saving connection string into secret manager
 */

resource "google_secret_manager_secret" "secret" {
  secret_id = local.secret_manager_secret_name

  replication {
    automatic = true
  }

  depends_on = [google_redis_instance.instance]
}

resource "google_secret_manager_secret_version" "secret_version_data" {
  secret      = google_secret_manager_secret.secret.name
  secret_data = "redis://${google_redis_instance.instance.host}:${google_redis_instance.instance.port}"

  depends_on = [google_secret_manager_secret.secret]
}