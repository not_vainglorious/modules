variable "project" {
  description = "Project identifier"
  type        = string
}

variable "network" {
  description = "Network name"
  type        = string
}

variable "name" {
  description = "Memorystore instance name"
  type        = string
}

variable "display_name" {
  description = "Memorystore display name"
  type        = string
}

variable "region" {
  description = "Memorystore region"
  type        = string
}

variable "tier" {
  description = "Memorystore tier"
  default     = "BASIC"
}

variable "memory_size_gb" {
  description = "Redis memory size in GiB"
  default     = 1
}