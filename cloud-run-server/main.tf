data "google_compute_network" "private_network" {
  name = var.network
}

data "google_project" "project" {}

resource "google_cloud_run_service" "server" {
  name     = var.name
  location = var.region

  template {
    spec {
      containers {
        image = "${var.image}:latest"

        command = var.container_command

        ports {
          container_port = var.port
        }

        dynamic "env" {
          for_each = var.envs

          content {
            name = env.key

            value_from {
              secret_key_ref {
                name = env.value
                key  = "latest"
              }
            }
          }
        }
      }
    }

    metadata {
      namespace = var.project

      annotations = {
        "autoscaling.knative.dev/maxScale"        = var.maximum_scaling
        "autoscaling.knative.dev/minScale"        = var.minimum_scaling
        "run.googleapis.com/vpc-access-connector" = var.vpc_connector
        "run.googleapis.com/vpc-access-egress"    = var.vpc_connector != null ? "private-ranges-only" : null
      }
    }
  }
}

resource "google_cloud_run_domain_mapping" "default" {
  location = var.region
  name     = var.domain_name

  spec {
    route_name = google_cloud_run_service.server.name
  }

  metadata {
    namespace = var.project
  }
}

resource "google_cloud_run_service_iam_member" "public_access" {
  count    = 1
  service  = google_cloud_run_service.server.name
  location = google_cloud_run_service.server.location
  project  = google_cloud_run_service.server.project
  role     = "roles/run.invoker"
  member   = "allUsers"
}