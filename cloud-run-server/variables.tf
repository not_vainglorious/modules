variable "project" {
  description = "Project identifier"
  type        = string
}

variable "network" {
  description = "Network name"
  type        = string
}

variable "name" {
  description = "Cloud run name"
  type        = string
}

variable "region" {
  description = "Cloud run region"
  type        = string
}

variable "container_command" {
  description = "Container command"
  type        = list(string)
}

variable "image" {
  description = "Image"
  type        = string
}

variable "port" {
  description = "Container port"
  type        = number
}

variable "minimum_scaling" {
  description = "Minimum scaling"
  type        = number
  default     = 1
}

variable "maximum_scaling" {
  description = "Maximum scaling"
  type        = number
  default     = 1
}

variable "envs" {
  description = "Map of environment variables from Secret Manager"
  type        = map(string)
  default     = {}
}

variable "vpc_connector" {
  description = "VPC access connector"
}

variable "domain_name" {
  description = "Domain name for Cloud Run Domain Mapping"
  type        = string
}