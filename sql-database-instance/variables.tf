variable "project" {
  description = "Project identifier"
  type        = string
}

variable "network" {
  description = "Network name"
  type        = string
}

variable "name" {
  description = "Database instance name"
  type        = string
}

variable "region" {
  description = "Database instance region"
  type        = string
}

variable "database_version" {
  description = "Database version"
}

variable "tier" {
  description = "Database tier"
}