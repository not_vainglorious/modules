locals {
  allocated_ip_range = "${var.name}-ip-alloc"
}

data "google_compute_network" "private_network" {
  name = var.network
}

resource "google_compute_global_address" "private_ip_alloc" {
  name          = local.allocated_ip_range
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = data.google_compute_network.private_network.id
}

resource "google_service_networking_connection" "cloud_sql_connection" {
  network                 = data.google_compute_network.private_network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_alloc.name]
}

resource "google_sql_database_instance" "instance" {
  project = var.project
  region  = var.region

  name             = var.name
  database_version = var.database_version

  settings {
    tier = var.tier
    ip_configuration {
      ipv4_enabled       = false
      private_network    = data.google_compute_network.private_network.id
      allocated_ip_range = local.allocated_ip_range
    }
  }

  depends_on = [google_service_networking_connection.cloud_sql_connection]
}
