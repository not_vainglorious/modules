output "name" {
  value = google_sql_database_instance.instance.name
}

output "connection" {
  value = google_sql_database_instance.instance.connection_name
}