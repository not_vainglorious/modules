
resource "google_compute_subnetwork" "subnet" {
  project = var.project
  region  = var.region
  network = var.network

  name                     = var.name
  ip_cidr_range            = var.ip_cidr_range
  private_ip_google_access = true

  dynamic "secondary_ip_range" {
    for_each = var.secondary_ip_ranges
    content {
      range_name    = secondary_ip_range.key
      ip_cidr_range = secondary_ip_range.value
    }
  }
}

resource "google_compute_router" "router" {
  name    = "${google_compute_subnetwork.subnet.name}-router"
  region  = google_compute_subnetwork.subnet.region
  network = var.network

  depends_on = [
    google_compute_subnetwork.subnet
  ]
}

resource "google_compute_router_nat" "nat_router" {
  name   = "${google_compute_subnetwork.subnet.name}-nat-router"
  region = google_compute_router.router.region
  router = google_compute_router.router.name

  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = google_compute_subnetwork.subnet.name
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }

  depends_on = [
    google_compute_router.router
  ]
}