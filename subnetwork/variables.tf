variable "project" {
  description = "Project identifier"
  type        = string
}

variable "network" {
  description = "Network name"
  type        = string
}

variable "name" {
  description = "Subnetwork name"
  type        = string
}

variable "region" {
  description = "Subnetwork region"
  type        = string
}

variable "ip_cidr_range" {
  description = "Subnetwork IP CIDR range"
}

variable "secondary_ip_ranges" {
  description = "Secondary IP ranges map"
  type        = map(string)
  default     = {}
}