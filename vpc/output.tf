output "name" {
  value = google_compute_network.vpc.name
}

output "vpc_access_connector" {
  value = google_vpc_access_connector.connector.name
}