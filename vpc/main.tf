
resource "google_compute_network" "vpc" {
  project = var.project

  name                            = var.name
  auto_create_subnetworks         = false
  routing_mode                    = "GLOBAL"
  delete_default_routes_on_create = true
  description                     = "VPC network (Managed by Terraform)"
}

resource "google_compute_route" "egress_internet" {
  network = google_compute_network.vpc.name

  name             = "${var.name}-egress-internet"
  dest_range       = "0.0.0.0/0"
  next_hop_gateway = "default-internet-gateway"

  depends_on = [
    google_compute_network.vpc
  ]
}

resource "google_vpc_access_connector" "connector" {
  name          = "${var.name}-vpc-connector"
  region        = var.region
  ip_cidr_range = "10.21.0.0/28"
  network       = google_compute_network.vpc.name
}
