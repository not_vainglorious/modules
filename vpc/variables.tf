variable "project" {
  description = "Project identifier"
  type        = string
}

variable "region" {
  description = "VPC access connector region"
  type        = string
}

variable "name" {
  description = "Network name"
}